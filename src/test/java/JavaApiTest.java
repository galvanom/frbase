import api.java.Store;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class JavaApiTest extends AbstractTest {

    @BeforeEach
    public void init() throws IOException {
        createTestFolder();
    }

    @Test
    public void basicFunctionsTest() {
        Store store = new Store(TEST_FOLDER_PATH);
        store.create("test1", "hello");
        Assertions.assertEquals("hello", store.read("test1"));
        store.update("test1", "bye");
        Assertions.assertEquals("bye", store.read("test1"));
        store.delete("test1");
        Assertions.assertNull(store.read("test1"));
    }

    @Test
    public void persistenceTest() {
        Store store = new Store(TEST_FOLDER_PATH);
        store.create("test1", "hello");
        store.create("test2", "hi");
        store.update("test1", "bye");
        store.create("test3", "deleteme");
        store.delete("test3");
        store.shutdown();

        Store anotherStore = new Store(TEST_FOLDER_PATH);
        Assertions.assertEquals("hi", anotherStore.read("test2"));
        Assertions.assertNull(anotherStore.read("test3"));
        Assertions.assertEquals("bye", anotherStore.read("test1"));
    }

}
