import inmemory.IncrementalRebuildMap;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import persistence.CommitLogger;

import java.io.IOException;
import java.util.Map;

public class CommitLoggerTest extends AbstractTest {

    @Test
    public void basicTest() throws Exception {

        CommitLogger commitLogger = new CommitLogger(TEST_FOLDER_PATH);
        commitLogger.writeToLog("test1", "testvalue1");
        commitLogger.writeToLog("test2", "testvalue");
        commitLogger.writeToLog("test1", "testvalue2");
        commitLogger.writeToLog("test2", "changedvalue");
        commitLogger.deleteRecord("test1");

        Map<String, Object> map = new IncrementalRebuildMap();
        commitLogger.readToMap(map);
        Assertions.assertFalse(map.isEmpty());
        Assertions.assertEquals(1, map.size(), "Size not equals");
        Assertions.assertTrue(map.containsKey("test2"), "No test2 key in map");
        Assertions.assertEquals( "changedvalue", map.get("test2"));
    }

    @BeforeEach
    public void init() throws IOException {
        createTestFolder();
    }

}
