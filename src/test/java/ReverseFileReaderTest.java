import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import persistence.ReverseFileReader;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class ReverseFileReaderTest {
    private final static byte[] SEPARATOR = "\n".getBytes();

    @Test
    public void singleSymbolTest() throws IOException {
        List<String> list = Arrays.asList("1", "2", "3", "4");
        List<byte[]> linesExpected = list.stream().map(String::getBytes).collect(Collectors.toList());
        test(getRawBytes(list), linesExpected);
    }

    @Test
    public void moreThanOneSymbolTest() throws IOException {
        List<String> list = Arrays.asList("1a", "2b", "3c", "4d");
        List<byte[]> linesExpected = list.stream().map(String::getBytes).collect(Collectors.toList());
        test(getRawBytes(list), linesExpected);
    }

    @Test
    public void noSeparatorAtTheEndTest() throws IOException {
        List<String> list = Arrays.asList("1a", "2b", "3c", "4d");
        List<byte[]> linesExpected = list.stream().map(String::getBytes).collect(Collectors.toList());
        byte[] rawBytes = getRawBytes(list);
        test(Arrays.copyOfRange(rawBytes, 0, rawBytes.length - SEPARATOR.length), linesExpected);
    }

    private byte[] getRawBytes(List<String> list) {
        List<byte[]> bytesList = list.stream().map(String::getBytes).collect(Collectors.toList());
        int bytesNumber = bytesList.stream()
                .map(x -> x.length)
                .reduce((x, y) -> x + y)
                .orElse(0);
        byte[] bytesToWrite = new byte[bytesNumber + list.size() * SEPARATOR.length];

        for (int i=0, pointer=0; i < bytesList.size(); i++) {
            byte[] bytes = bytesList.get(i);
            System.arraycopy(bytes, 0, bytesToWrite, pointer, bytes.length);
            pointer += bytes.length;
            System.arraycopy(SEPARATOR, 0, bytesToWrite, pointer, SEPARATOR.length);
            pointer += SEPARATOR.length;
        }

        return bytesToWrite;
    }

    public void test(byte[] bytes, List<byte[]> linesExpected) throws IOException {
        List<String> reversedLines = new ArrayList<>();
        File file = getFileToTest(bytes);
        try (ReverseFileReader fileReader = new ReverseFileReader(file, SEPARATOR)) {
            fileReader.setBufferSize(5);
            String line;
            int count = linesExpected.size();
            while ((line = fileReader.readLine()) != null) {
                Assertions.assertTrue(count >= 0, "Too many lines");
                System.out.println(line);
                reversedLines.add(line);
                count--;
            }
        }

        Assertions.assertEquals(linesExpected.size(), reversedLines.size());
        Collections.reverse(reversedLines);
        for (int i = 0; i < linesExpected.size(); i++) {
            if (!new String(linesExpected.get(i)).equals(new String(reversedLines.get(i)))) {
                Assertions.fail("Not equals");
            }
        }
    }

    private File getFileToTest(byte[] bytes) throws IOException {
        File file = new File("reverse_test.dat");
        if (file.exists() && !file.delete()) {
            throw new RuntimeException("Can't delete test file");
        }
        if (!file.createNewFile()) {
            throw new RuntimeException("Can't create test file");
        }
        try (FileOutputStream fos = new FileOutputStream(file)){
            fos.write(bytes);
        }
        return file;
    }

}
