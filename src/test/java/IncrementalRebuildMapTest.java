import inmemory.IncrementalRebuildMap;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

public class IncrementalRebuildMapTest {

    @Test
    public void minFunctionalityTest() {
        IncrementalRebuildMap map = new IncrementalRebuildMap();
        map.put("test1", "45");
        map.put("test2", 4);

        Assertions.assertTrue(map.containsKey("test1"));
        Assertions.assertTrue(map.containsKey("test2"));
        Assertions.assertFalse(map.containsKey("hhhhdhdhdhd"));

        Assertions.assertEquals(map.get("test1"), "45");
        Assertions.assertEquals(map.get("test2"), 4);

        map.remove("test1");
        Assertions.assertFalse(map.containsKey("test1"));
    }

    @Test
    public void rebuildTest() {
        IncrementalRebuildMap map = new IncrementalRebuildMap(10, 1.0f, 10);
        int numberToInsert = 1000;
        IntStream.range(0, numberToInsert).forEach(i -> map.put("test" + i, i));
        Assertions.assertEquals(numberToInsert, map.size());
        Assertions.assertTrue(IntStream.range(0, numberToInsert).mapToObj(i -> "test" + i).allMatch(map::containsKey));
    }

}
