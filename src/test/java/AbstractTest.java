import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public abstract class AbstractTest {
    protected static final String TEST_FOLDER_PATH = "/home/otto/test";

    protected void createTestFolder() throws IOException {
        File file = new File(TEST_FOLDER_PATH);
        deleteTestFolder();
        if (!file.mkdir()) {
            throw new RuntimeException("Can't create dir " + TEST_FOLDER_PATH);
        }
    }

    protected void deleteTestFolder() throws IOException {
        File file = new File(TEST_FOLDER_PATH);
        if (Files.exists(file.toPath())) {
            Files.walk(file.toPath())
                    .map(Path::toFile)
                    .forEach(File::delete);
            if (!Files.deleteIfExists(file.toPath())) {
                throw new RuntimeException("Can't delete dir " + TEST_FOLDER_PATH);
            }
        }
    }


}
