import api.java.Store;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.stream.IntStream;

public class DurabilityAndPerfomanceTest extends AbstractTest {
    private static final int KEYS_NUMBER = 100000;

    @BeforeEach
    public void init() throws IOException {
        createTestFolder();
    }

    @Test
    public void test() {
        String keyPrefix = "key";
        String valuePrefix = "valuevaluevaluevaluevaluevaluevaluevaluevaluevaluevaluevaluevaluevaluevaluevalue";
        int printEvery = 1000;
        Store store1 = new Store(TEST_FOLDER_PATH);
        System.out.println("Start loading...");
        long time1 = System.currentTimeMillis();
        IntStream.range(0, KEYS_NUMBER)
                .peek(i -> {if (i % printEvery == 0) System.out.println(i);})
                .forEach(i -> store1.create(keyPrefix + i, valuePrefix + i));
        System.out.println("Load time of " + KEYS_NUMBER + " keys : " + (System.currentTimeMillis() - time1));
        store1.shutdown();

        System.out.println("Start reading from disk...");
        long time2 = System.currentTimeMillis();
        Store store2 = new Store(TEST_FOLDER_PATH);
        System.out.println("Read from disk : " + (System.currentTimeMillis() - time2));
        System.out.println("Start reading keys from store...");
        long time3 = System.currentTimeMillis();
        long c = IntStream.range(0, KEYS_NUMBER)
                .peek(i -> {if (i % printEvery == 0) System.out.println(i);})
                .filter(i -> {
                            String value = store2.read(keyPrefix + i);
                            return value != null && value.equals(valuePrefix + i);
                        }
                )

                .count();
        System.out.println("Read time of " + KEYS_NUMBER + " keys : " + (System.currentTimeMillis() - time3));
        Assertions.assertEquals(KEYS_NUMBER, c);

    }



}
