package api.java;

import inmemory.IncrementalRebuildMap;
import persistence.CommitLogger;
import persistence.PersistenceException;

import java.io.File;
import java.util.Map;

public class Store {
    private final String basePath;
    private final CommitLogger commitLogger;
    private Map map;
    private boolean shutdown;

    public Store(String basePath) {
        this.basePath = basePath;
        this.commitLogger = new CommitLogger(basePath);
        this.map = new IncrementalRebuildMap();
        File file = new File(basePath);
        if (file.exists()) {
            try {
                this.commitLogger.readToMap(this.map);
            } catch (PersistenceException e) {
                throw new RuntimeException(e);
            }
        } else {
            file.mkdir();
        }
    }

    public void create(String key, String value) {
        this.update(key, value);
    }

    public void update(String key, String value) {
        checkState();
        map.put(key, value);
        try {
            commitLogger.writeToLog(key, value);
        } catch (PersistenceException e) {
            throw new RuntimeException(e);
        }
    }

    public String read(String key) {
        checkState();
        return (String) map.get(key);
    }

    public boolean delete(String key) {
        checkState();
        if (map.remove(key) == null) {
            return false;
        }
        try {
            commitLogger.deleteRecord(key);
        } catch (PersistenceException e) {
            throw new RuntimeException(e);
        }
        return true;
    }

    private void checkState() {
        if (isShutdown()) {
            throw new IllegalStateException("Store shutodwn");
        }
    }

    public boolean isShutdown() {
        return shutdown;
    }

    public void shutdown() {
        shutdown = true;
        map.clear();
        map = null;
    }

}
