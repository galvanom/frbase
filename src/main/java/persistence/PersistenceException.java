package persistence;

public class PersistenceException extends Exception {
    public PersistenceException(Throwable throwable) {
        super(throwable);
    }
}
