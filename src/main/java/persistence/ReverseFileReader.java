package persistence;

import java.io.*;

public class ReverseFileReader implements Closeable {
    private final File file;
    private RandomAccessFile randomAccessFile;
    private byte[] lineSeparator;
    private int bufferSize = 4096 * 2;
    private int bufferPointer = -1;
    private byte[] buffer;
    private long filePointer;
    private ByteLineStream byteLineStream;

    public ReverseFileReader(File file) throws IOException {
        this(file, "\n".getBytes());
    }

    public ReverseFileReader(File file, byte[] lineSeparator) throws IOException {
        this.lineSeparator = lineSeparator;
        this.file = file;
        this.randomAccessFile = new RandomAccessFile(file, "r");
        this.filePointer = file.length();
        this.byteLineStream = new ByteLineStream();
        this.byteLineStream.close();
    }

    public String readLine() throws IOException {
        if (bufferPointer < 0) {
            if (!loadBufferFromFile()) {
                if (!byteLineStream.isClosed) {
                    byteLineStream.close();
                    return new String(byteLineStream.getBytesReversed());
                }
                return null;
            }
        }
        byte[] line = getLineFromBuffer();
        return line == null ? readLine() : new String(line);
    }


    private boolean loadBufferFromFile() throws IOException {
        if (filePointer <= 0) {
            return false;
        }
        if (filePointer-bufferSize < 0) {
            buffer = new byte[(int) filePointer];
            filePointer = 0;
        } else {
            buffer = new byte[bufferSize];
            filePointer = filePointer - bufferSize;
        }
        randomAccessFile.seek(filePointer);
        bufferPointer = randomAccessFile.read(buffer) - 1;
        return true;
    }

    private byte[] getLineFromBuffer() {
        readNextLineFromBuffer();
        return byteLineStream.isClosed ? byteLineStream.getBytesReversed() : null;
    }

    private void readNextLineFromBuffer() {
        if (byteLineStream.isClosed) {
            jumpOverLineSeparator();
            byteLineStream = new ByteLineStream();
        }
        for ( ; bufferPointer >= 0; bufferPointer--) {
            if (isLineSeparator(bufferPointer) || byteLineStream.removeSeparator(lineSeparator)) {
                byteLineStream.close();
                break;
            }
            byteLineStream.addByte(buffer[bufferPointer]);
        }
    }

    private void jumpOverLineSeparator() {
        if (isLineSeparator(bufferPointer)) {
            bufferPointer -= lineSeparator.length;
        }
    }

    private boolean isLineSeparator(int currentBufferPointer) {
        if (currentBufferPointer < lineSeparator.length - 1) {
            return false;
        }
        for (int i = currentBufferPointer, j = lineSeparator.length-1 ; i >= 0 && j >= 0; i--, j--) {
            if (buffer[i] != lineSeparator[j]) {
                return false;
            }
        }
        return true ;
    }

    public int getBufferSize() {
        return bufferSize;
    }

    public void setBufferSize(int bufferSize) {
        this.bufferSize = bufferSize;
    }

    @Override
    public void close() throws IOException {
        randomAccessFile.close();
    }
}
