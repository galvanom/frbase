package persistence;

import persistence.record.LogRecord;
import persistence.record.LogRecordType;
import persistence.record.SimpleLogRecord;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.util.*;

public class CommitLogger {
    private final String logPath;

    public CommitLogger(String logPath) {
        this.logPath = logPath;
    }

    public void writeToLog(String key, Object value) throws PersistenceException {
        if (key == null || value == null) {
            throw new IllegalArgumentException("Key: " + key + " Value: " + value);
        }
        addToLog(new SimpleLogRecord(LogRecordType.WRITE, key, value.toString()));
    }

    public void deleteRecord(String key) throws PersistenceException {
        if (key == null) {
            throw new IllegalArgumentException("Key is NULL");
        }
        addToLog(new SimpleLogRecord(LogRecordType.DELETE, key));
    }

    private void addToLog(LogRecord logRecord) throws PersistenceException {
        try {
            FileOutputStream fos = new FileOutputStream(getLastLogFile(), true);
            fos.write(logRecord.getRecord());

        } catch (IOException e) {
            throw new PersistenceException(e);
        }
    }

    public void compactLog() {
        throw new UnsupportedOperationException();
    }

    public void readToMap(Map mapToRead) throws PersistenceException {
        Set<String> deletedKeysCache = new HashSet<>();
        try (ReverseFileReader rfr = new ReverseFileReader(getLastLogFile())) {
            if (getLastLogFile().length() == 0) { // TODO: 18.12.18 CRUNCH!!!
                return;
            }
            String line;
            while ((line = rfr.readLine()) != null) {
                LogRecord logRecord = SimpleLogRecord.valueOf(line);
                switch (logRecord.getType()) {
                    case DELETE:
                        if (!deletedKeysCache.contains(logRecord.getKey())) {
                            deletedKeysCache.add(logRecord.getKey());
                        }
                        break;
                    case WRITE:
                        if (!deletedKeysCache.contains(logRecord.getKey()) && !mapToRead.containsKey(logRecord.getKey())) {
                            mapToRead.put(logRecord.getKey(), logRecord.getValue());
                        }
                        break;
                    default:
                }
            }
        } catch (ParseException | IOException e) {
            throw new PersistenceException(e);
        }

    }

    private File getLastLogFile() throws IOException {
        Comparator<String> comparator = new LogFileNameComparator();
        Path path = new File(logPath).toPath();
        String fileName = Files.list(path)
                .map(Path::toFile)
                .map(File::getName)
                .filter(name -> name.startsWith("log"))
                .filter(name -> name.endsWith(".dat"))
                .max(comparator)
                .orElse("log1.dat");
        File file = new File(logPath, fileName);
        if (!file.exists()) {
            file.createNewFile();
        }
        return file;
    }


    private static class LogFileNameComparator implements Comparator<String> {
        @Override
        public int compare(String name1, String name2) {
            return Integer.compare(getLogNumber(name1), getLogNumber(name2));
        }

        private int getLogNumber(String name) {
            String strNumber = name.replace("log", "")
                    .replace(".dat", "");
            return Integer.valueOf(strNumber);
        }
    }
}
