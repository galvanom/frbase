package persistence;


public class TestByteArray {
    public static void main(String[] args) {
        ByteLineStream byteLineStream = new ByteLineStream();
        byteLineStream.addByte(0xff);
        byteLineStream.addByte(0x00);
        for (byte b : byteLineStream.getBytesReversed()) {
            System.out.printf("%x", b);
        }


    }
}
