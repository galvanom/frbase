package persistence.record;

import java.text.ParseException;

public class SimpleLogRecord implements LogRecord {
    private LogRecordType logRecordType;
    private String key;
    private String value;

    public SimpleLogRecord(LogRecordType logRecordType, String key) {
        this.logRecordType = logRecordType;
        this.key = key;
    }

    public SimpleLogRecord(LogRecordType logRecordType, String key, String value) {
        this.logRecordType = logRecordType;
        this.key = key;
        this.value = value;
    }

    @Override
    public LogRecordType getType() {
        return logRecordType;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public byte[] getRecord() { // TODO: 12.12.18 Change separator, remove line separator
        String stringRepresentation = logRecordType.name() + "_" + getKey();
        if (logRecordType == LogRecordType.WRITE) {
            stringRepresentation += "_" + getValue();
        }
        return (stringRepresentation + "\n").getBytes();
    }

    public static LogRecord valueOf(String string) throws ParseException {
        String[] splitted = string.split("_");
        if (string.startsWith(LogRecordType.WRITE.name())) {
            return new SimpleLogRecord(LogRecordType.WRITE, splitted[1], splitted[2]);
        } else if (string.startsWith(LogRecordType.DELETE.name())) {
            return new SimpleLogRecord(LogRecordType.DELETE, splitted[1]);
        } else {
            throw new ParseException("Can't parse log record " + string, 0);
        }
    }
}
