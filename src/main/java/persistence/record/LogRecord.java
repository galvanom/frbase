package persistence.record;

public interface LogRecord {
    LogRecordType getType();
    String getKey();
    String getValue();
    byte[] getRecord();
}
