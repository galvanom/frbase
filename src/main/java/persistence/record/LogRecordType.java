package persistence.record;

public enum LogRecordType {
    WRITE, DELETE;
}
