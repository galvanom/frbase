package persistence;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

// TODO: 18.12.18 Test
public class ByteLineStream extends ByteArrayOutputStream {
    boolean isClosed;

    public void addByte(int b) {
        if (isClosed) {
            throw new IllegalStateException("Closed. Can't add bytes");
        }
        write(b);
    }

    public boolean isClosed() {
        return isClosed;
    }

    @Override
    public void close() {
        try {
            super.close();
        } catch (IOException e) {
            throw new RuntimeException(e); // TODO: 18.12.18
        }
        isClosed = true;
    }

    public byte[] getBytes() {
        return toByteArray();
    }

    public byte[] getBytesReversed() {
        byte[] bytes = getBytes();
        if (bytes.length == 0) {
            return bytes;
        }
        ByteArrayOutputStream reversed = new ByteArrayOutputStream();
        for(int i=bytes.length-1; i>=0; i-- ) {
            reversed.write(bytes[i]);
        }
        return reversed.toByteArray();
    }

    public boolean removeSeparator(byte[] separator) {
        if (!endsWith(separator)) {
            return false;
        }
        this.count -= separator.length;
        return true;
    }

    private boolean endsWith(byte[] tail) {
        if (this.count < tail.length) {
            return false;
        }
        for (int i = this.count - tail.length, j = tail.length - 1; i < this.count && j >= 0; i++, j-- ) {
            if (buf[i] != tail[j]) {
                return false;
            }
        }

        return true;
    }



}
