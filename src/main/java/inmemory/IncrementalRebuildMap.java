package inmemory;

import java.util.*;

// TODO: 17.12.18 Follow the interface contract for return values
public class IncrementalRebuildMap implements Map<String, Object> {
    private final StringHash stringHash;
    private Node[] buckets;
    private final float maxLoadFactor;
    private IncrementalRebuildMap rebuildMap;
    private int elementsToCopyNumber;
    private boolean dontRebuild;
    private int size;

    public IncrementalRebuildMap() {
        this(16, 0.7f, 10);
    }

    public IncrementalRebuildMap(int size, float maxLoadFactor, int elementsToCopyNumber) {
        this(size, maxLoadFactor, elementsToCopyNumber, new DefaultStringHash());
    }

    public IncrementalRebuildMap(int size, float maxLoadFactor, int elementsToCopyNumber, StringHash stringHash) {
        this.buckets = new Node[size];
        this.maxLoadFactor = maxLoadFactor;
        this.elementsToCopyNumber = elementsToCopyNumber;
        this.stringHash = stringHash;
    }

    @Override
    public int size() {
        return size + (rebuildMap == null ? 0 : rebuildMap.size());
    }

    @Override
    public boolean isEmpty() {
        return this.size == 0;
    }

    @Override
    public boolean containsKey(Object o) {
        return findNode((String) o) != null;
    }

    @Override
    public boolean containsValue(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object get(Object o) {
        return Optional.ofNullable(findNode((String) o))
                .map(Node::getValue)
                .orElse(null);
    }

    @Override
    public Object put(String s, Object o) {
        if (rebuildMap != null) {
            rebuildMap.put(s, o);
            copyEntriesToRebuildMap();
            if (isEmpty()) {
                buckets = rebuildMap.buckets;
                size = rebuildMap.size;
                rebuildMap = null;
            }
            return o;
        } else if (getLoadFactor() > maxLoadFactor && !dontRebuild) {
            initRebuildMap();
        }
        int bucketNumber = getBucketNumber(s);
        if (buckets[bucketNumber] == null) {
            buckets[bucketNumber] = new Node(s, o);
            size++;
        }
        else {
           Node node = findNode(buckets[bucketNumber], s);
           if (node == null) {
               node = new Node(s, o);
               node.next = buckets[bucketNumber];
               buckets[bucketNumber] = node;
               size++;
           } else {
               node.value = o;
           }
        }
        return o;
    }

    private Node findNode(String key) {
        Node node;
        if (rebuildMap != null && (node = rebuildMap.findNode(key)) != null) {
            return node;
        }
        int bucketNumber = getBucketNumber(key);

        return buckets[bucketNumber] == null ? null : findNode(buckets[bucketNumber], key);
    }

    int max = 0;
    private Node findNode(Node node, String key) {
        int i = 0;
        do {
            if (node.getKey().equals(key)) {
                return node;
            }
            node = node.next;
            i++;
            if (i > max) {
                max = i;
//                System.out.println(i);
            }
        } while (node != null);
        return null;
    }

    @Override
    public Object remove(Object o) {
        if (rebuildMap != null) { // todo return value if key is in rebuildMap but not in this map
            rebuildMap.remove(o);
        }
        String key = (String) o;
        int bucketNumber = getBucketNumber(key);
        Node node = buckets[bucketNumber];
        if (node == null) {
            return null;
        }
        Node prevNode = null;
        do {
            if (node.getKey().equals(key)) {
                if (prevNode == null) {
                    buckets[bucketNumber] = node.next;
                } else {
                    prevNode.next = node.next;
                }
                node.next = null;
                size--;
                return node.getValue();
            }
            prevNode = node;
            node = node.next;
        } while (node != null);

        return null;
    }

    @Override
    public void putAll(Map map) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        if (rebuildMap != null) {
            rebuildMap.clear();
        }
        for (int i = 0; i < buckets.length; i++) {
            buckets[i] = null;
        }
        size = 0;
    }

    @Override
    public Set<String> keySet() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Collection<Object> values() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Set<Entry<String, Object>> entrySet() {
        throw new UnsupportedOperationException();
    }

    private int getBucketNumber(String s) {
        int hashCode = hashString(s);
        return Math.abs(hashCode % buckets.length);
    }

    private int hashString(String s) {
        return stringHash.getHash(s);
    }

    private float getLoadFactor() {
        return size / buckets.length;
    }

    private void copyEntriesToRebuildMap() {
        int elementsCopied = 0;
        for (int i = 0; i < buckets.length ; i++) {
            for (Node node = buckets[i]; node != null; node = node.next) {
                if (!rebuildMap.containsKey(node.getKey())) {
                    rebuildMap.put(node.getKey(), node.getValue());
                    elementsCopied++;
                }
                size--;
                buckets[i] = node.next;
                if (elementsCopied > elementsToCopyNumber) {
                    return;
                }
            }
        }
    }

    private void initRebuildMap() {
        rebuildMap =  new IncrementalRebuildMap(buckets.length * 2,
                this.maxLoadFactor, this.elementsToCopyNumber);
        rebuildMap.dontRebuild = true;
    }

    private static class Node {
        private final String key;
        private Object value;
        private Node next;

        public Node(String key, Object value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public Object getValue() {
            return value;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }
    }


}
