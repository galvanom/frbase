package inmemory;

public interface StringHash {
    int getHash(String s);
}
