package inmemory;

public class DefaultStringHash implements StringHash {
    @Override
    public int getHash(String s) {
        return s.hashCode();
    }
}
